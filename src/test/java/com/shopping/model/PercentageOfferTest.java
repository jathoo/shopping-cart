package com.shopping.model;

import com.shopping.cart.model.PercentageOffer;
import com.shopping.cart.model.Product;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class PercentageOfferTest {

    PercentageOffer percentageOffer;
    private Product product1;
    private Product product2;

    @Before
    public void setUp() {
        percentageOffer = new PercentageOffer();
        product1 = new Product("apple", new BigDecimal("1.00"));
        product2 = new Product("soup", new BigDecimal("0.65"));

        Map<Product,Integer> CompulsoryProducts = new HashMap<>();
        CompulsoryProducts.put(product2,2);

        percentageOffer.setProduct(product1);
        percentageOffer.setDiscount(new BigDecimal(0.2));
        percentageOffer.setCompulsoryProducts(CompulsoryProducts);

    }

    @Test
    public void calculateOfferAmountTest() {
        assertEquals(new BigDecimal("0.20"),percentageOffer.calculateOfferAmount().setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void canApplyOfferTest() {
        Map<Product, Integer> numberOfProducts = new HashMap<>();
        numberOfProducts.put(product2,2);
        assertTrue(percentageOffer.canApplyOffer(numberOfProducts));
    }

    @Test
    public void canNotApplyOfferTest(){
        Map<Product, Integer> numberOfProducts = new HashMap<>();
        numberOfProducts.put(product2,1);
        assertFalse(percentageOffer.canApplyOffer(numberOfProducts));
    }

    @Test
    public void messageBuilderTest() {
        assertEquals("apple 20% off -0.40",percentageOffer.messageBuilder("0.40"));
    }

}
