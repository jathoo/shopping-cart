package com.shopping.service;

import com.shopping.cart.model.*;
import com.shopping.cart.service.ShoppingCartServiceImpl;
import com.shopping.cart.service.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class ShoppingCartServiceTest {
    private ShoppingCartServiceImpl basketService;
    private ProductServiceImpl productService;
    private Cart cart;
    private Product product1;
    private Product product2;
    private Product product3;
    private Product product4;


    @Before
    public void setUp() {
        productService = new ProductServiceImpl();
        basketService = new ShoppingCartServiceImpl(productService);

        product1 = new Product("apple", new BigDecimal("1.00"));
        product2 = new Product("soup", new BigDecimal("0.65"));
        product3 = new Product("milk", new BigDecimal("1.30"));
        product4 = new Product("bread", new BigDecimal("0.80"));

        createBasket(product1, product2, product3);
        cart.addProduct(product4);

        addProductsToProductService(product1);
        addProductsToProductService(product2);
        addProductsToProductService(product3);
        addProductsToProductService(product4);
        addProductsToProductService(product4);
    }

    private void addProductsToProductService(Product product) {
        productService.products.put(product.getId(), product);
    }

    private void addOffersToProductService(Offer offer) {
        productService.offers.put(offer.getId(), offer);
    }



    @Test
    public void addProductsToBasketTest() {
        //when
        String[] products = new String[]{"apple", "soup", "milk", "bread"};

        //then
        assertEquals(cart, basketService.addProdcutsToCart(products));

    }

    @Test
    public void calculateBasketPricesWithOutOfferTest() {
        //given
        BillingDetail billingDetail = new BillingDetail();
        billingDetail.setTotal(new BigDecimal(3.75));
        billingDetail.setSubTotal(new BigDecimal(3.75));
        billingDetail.setOfferTotals(new HashMap<>());

        //when
        BillingDetail billingDetailResult = basketService.calculateCartPrice(cart);


        //then
        assertEquals(billingDetailResult, billingDetailResult);
    }

    @Test
    public void calculateBasketPricesWithSingleOfferTest() {

        //given
        PercentageOffer offer = new PercentageOffer();
        offer.setId("singleOffer");
        offer.setDiscount(new BigDecimal("0.1"));
        offer.setProduct(product1);
        addOffersToProductService(offer);

        Map<Offer,BigDecimal> expectedOfferTotal = new HashMap<>();
        expectedOfferTotal.put(offer,offer.getDiscount().setScale(3,RoundingMode.HALF_UP));

        //when
        BillingDetail billingDetailResult = basketService.calculateCartPrice(cart);

        //then
        assertEquals(new BigDecimal(3.65).setScale(3, RoundingMode.HALF_UP),billingDetailResult.getTotal());
        assertEquals(new BigDecimal(3.75).setScale(2, RoundingMode.HALF_UP),billingDetailResult.getSubTotal());
        assertEquals(expectedOfferTotal,billingDetailResult.getOfferTotals());

    }


    @Test
    public void calculateBasketPricesWithMultiOfferTest() {

        //given
        Map<Product,Integer> requiredProduct = new HashMap<>();
        requiredProduct.put(product2,2);

        createBasket(product2, product1, product2);


        PercentageOffer offer = new PercentageOffer();
        offer.setId("multiOffer");
        offer.setDiscount(new BigDecimal("0.5"));
        offer.setProduct(product1);
        offer.setCompulsoryProducts(requiredProduct);
        addOffersToProductService(offer);

        Map<Offer,BigDecimal> expectedOfferTotal = new HashMap<>();
        expectedOfferTotal.put(offer,offer.getDiscount().setScale(3,RoundingMode.HALF_UP));

        //when
        BillingDetail billingDetailResult = basketService.calculateCartPrice(cart);

        //then
        assertEquals(new BigDecimal(1.80).setScale(3, RoundingMode.HALF_UP),billingDetailResult.getTotal());
        assertEquals(new BigDecimal(2.30).setScale(2, RoundingMode.HALF_UP),billingDetailResult.getSubTotal());
        assertEquals(expectedOfferTotal,billingDetailResult.getOfferTotals());

    }

    @Test
    public void calculateBasketPricesWithMultiOfferExpiredTest() {

        //given
        Map<Product,Integer> requiredProduct = new HashMap<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

        requiredProduct.put(product2,2);

        createBasket(product2, product1, product2);


        PercentageOffer offer = new PercentageOffer();
        offer.setId("multiOffer");
        offer.setDiscount(new BigDecimal("0.5"));
        offer.setProduct(product1);
        offer.setExpiryDate(formatter.format(LocalDate.now().minusDays(5)));
        offer.setCompulsoryProducts(requiredProduct);
        addOffersToProductService(offer);

        //when
        BillingDetail billingDetailResult = basketService.calculateCartPrice(cart);

        //then
        assertEquals(new BigDecimal(2.30).setScale(2, RoundingMode.HALF_UP),billingDetailResult.getTotal());
        assertEquals(new BigDecimal(2.30).setScale(2, RoundingMode.HALF_UP),billingDetailResult.getSubTotal());
        assertEquals(new HashMap<>(),billingDetailResult.getOfferTotals());

    }


    @Test
    public void calculateBasketPricesWithMultiOfferNotExpiredTest() {

        //given
        Map<Product,Integer> requiredProduct = new HashMap<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

        requiredProduct.put(product2,2);

        createBasket(product2, product1, product2);


        PercentageOffer offer = new PercentageOffer();
        offer.setId("multiOffer");
        offer.setDiscount(new BigDecimal("0.5"));
        offer.setProduct(product1);
        offer.setExpiryDate(formatter.format(LocalDate.now().plusDays(5)));
        offer.setCompulsoryProducts(requiredProduct);
        addOffersToProductService(offer);


        Map<Offer,BigDecimal> expectedOfferTotal = new HashMap<>();
        expectedOfferTotal.put(offer,offer.getDiscount().setScale(3,RoundingMode.HALF_UP));

        //when
        BillingDetail billingDetailResult = basketService.calculateCartPrice(cart);

        //then
        assertEquals(new BigDecimal(1.80).setScale(3, RoundingMode.HALF_UP),billingDetailResult.getTotal());
        assertEquals(new BigDecimal(2.30).setScale(2, RoundingMode.HALF_UP),billingDetailResult.getSubTotal());
        assertEquals(expectedOfferTotal,billingDetailResult.getOfferTotals());

    }

    private void createBasket(Product product2, Product product1, Product product22) {
        cart = new Cart();
        cart.addProduct(product2);
        cart.addProduct(product1);
        cart.addProduct(product22);
    }


}
