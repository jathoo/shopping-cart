package com.shopping.service;

import com.shopping.cart.model.Offer;
import com.shopping.cart.model.PercentageOffer;
import com.shopping.cart.model.Product;
import com.shopping.cart.service.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class ProductServiceImplTest {

    private ProductServiceImpl productService;
    private Product product1;


    @Before
    public void setUp() {
        productService = new ProductServiceImpl();
        product1 = new Product("apple", new BigDecimal("1.00"));
        addProductToProductService(product1);
    }

    private void addProductToProductService(Product product) {
        productService.products.put(product.getId(), product);
    }

    private void addOffersToProductService(Offer offer) {
        productService.offers.put(offer.getId(), offer);
    }


    @Test
    public void getProductTest() {
        //when
        Optional<Product> product = productService.getProduct("apple");
        Optional<Product> expected = Optional.of(product1);

        //then
        assertEquals(expected, product);
    }

    @Test
    public void getEmptyProductTest() {
        //when
        Optional<Product> product = productService.getProduct("xapple");

        //then
        assertEquals(Optional.empty(), product);
    }

    @Test
    public void getOffersTest() {
        //when
        PercentageOffer offer = new PercentageOffer();
        offer.setId("singleOffer");
        offer.setDiscount(new BigDecimal("0.1"));
        offer.setProduct(product1);
        addOffersToProductService(offer);

        List<Offer> offers = productService.getOffers(product1);
        List<Offer> expectedOffers = new ArrayList<>();
        expectedOffers.add(offer);

        //then
        assertEquals(expectedOffers, offers);
        assertEquals(1,offers.size());
    }


}
