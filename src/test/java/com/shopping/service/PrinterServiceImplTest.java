package com.shopping.service;

import com.shopping.cart.model.Offer;
import com.shopping.cart.model.PercentageOffer;
import com.shopping.cart.model.Product;
import com.shopping.cart.service.PrinterServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class PrinterServiceImplTest {
    private PrinterServiceImpl printerService;
    private Product product1;


    @Before
    public void setUp() {

        printerService = new PrinterServiceImpl();
        product1 = new Product("apple", new BigDecimal("1.00"));
    }

    @Test
    public void testToCurrency() {
        String formatted = printerService.toCurrency(new BigDecimal(3.00));
        assertEquals("£3.00",formatted);
    }

    @Test
    public void tenPercentageOfferTest() {
        PercentageOffer offer = new PercentageOffer();
        offer.setId("singleOffer");
        offer.setDiscount(new BigDecimal("0.1"));
        offer.setProduct(product1);
        Map<Offer,BigDecimal> offerBigDecimalMap = new HashMap<>();
        offerBigDecimalMap.put(offer,new BigDecimal("0.4"));

       assertEquals("apple 10% off -£0.40",printerService.prettyPrintOffers(offerBigDecimalMap).trim());
    }

    @Test
    public void twentyPercentageOfferTest() {
        PercentageOffer offer = new PercentageOffer();
        offer.setId("singleOffer");
        offer.setDiscount(new BigDecimal("0.2"));
        offer.setProduct(product1);
        Map<Offer,BigDecimal> offerBigDecimalMap = new HashMap<>();
        offerBigDecimalMap.put(offer,new BigDecimal("0.5"));

        assertEquals("apple 20% off -£0.50",printerService.prettyPrintOffers(offerBigDecimalMap).trim());
    }


    @Test
    public void emptyOfferTest() {
        assertEquals("no offers available",printerService.prettyPrintOffers(null).trim());
    }



}
