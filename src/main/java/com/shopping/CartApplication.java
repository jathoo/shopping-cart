package com.shopping;

import com.shopping.cart.ShoppingCartManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CartApplication {

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ShoppingCartManager.class);
        ShoppingCartManager cartManager = ctx.getBean(ShoppingCartManager.class);
        cartManager.setProductNames(args);
        cartManager.execute();
    }
}
