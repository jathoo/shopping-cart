package com.shopping.cart.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class BillingDetail {
    private BigDecimal subTotal;
    private BigDecimal total;
    private Map<Offer, BigDecimal> offerTotals;

}
