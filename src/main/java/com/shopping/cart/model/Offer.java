package com.shopping.cart.model;

import java.math.BigDecimal;
import java.util.Map;

public interface Offer {
    String getId();
    Product getProduct();
    BigDecimal calculateOfferAmount();
    boolean canApplyOffer(Map<Product, Integer> numberOfProductsInCart);
    String messageBuilder(String totalDiscount);

}
