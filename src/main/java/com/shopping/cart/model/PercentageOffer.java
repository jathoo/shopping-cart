package com.shopping.cart.model;

import com.shopping.cart.service.ProductServiceImpl;
import lombok.Data;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Data
public class PercentageOffer implements Offer {
    private String id;
    private BigDecimal discount;
    private Product product;
    private Map<Product, Integer> compulsoryProducts;
    private String expiryDate;

    @Inject
    ProductServiceImpl productService;


    @Override
    public BigDecimal calculateOfferAmount() {
        return product.getPrice().multiply(discount);
    }

    @Override
    public boolean canApplyOffer(Map<Product, Integer> numberOfProductsInCart) {
        return isNotExpired(expiryDate) && hasCompulsoryProducts(numberOfProductsInCart);
    }

    private boolean isNotExpired(String expiryDate) {
        if (expiryDate == null) {
            return true;
        }
        return formatExpiryDate().isAfter(LocalDate.now());
    }

    private LocalDate formatExpiryDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
        return LocalDate.parse(expiryDate, formatter);
    }

    @Override
    public String messageBuilder(String totalDiscount) {
        return product.getId() + " "+  discountToPercentage() + "%"  + " "+ "off" + " -"+  totalDiscount;
    }

    private String discountToPercentage() {
        BigDecimal displayablePercent = discount.multiply(new BigDecimal(100));
        DecimalFormat format = new DecimalFormat("#.##");
        return format.format(displayablePercent.doubleValue());
    }

    private boolean hasCompulsoryProducts(Map<Product, Integer> numberOfProductsInCart) {
        if (compulsoryProducts == null || compulsoryProducts.isEmpty()) {
            return true;
        }

        for (Map.Entry<Product, Integer>  compulsoryProductEntry : compulsoryProducts.entrySet()) {
            Product requiredProduct =  compulsoryProductEntry.getKey();
            Integer qty =  compulsoryProductEntry.getValue();
            Integer productNumber = numberOfProductsInCart.get(requiredProduct);
            if (productNumber != null && productNumber >= qty) {
                numberOfProductsInCart.put(requiredProduct, productNumber - qty);
            } else {
                return false;
            }
        }
        return true;
    }

}
