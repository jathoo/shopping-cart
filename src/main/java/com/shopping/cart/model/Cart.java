package com.shopping.cart.model;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class Cart {
    private List<Product> products = new LinkedList<>();

    public void addProduct(Product product) {
        products.add(product);
    }
}
