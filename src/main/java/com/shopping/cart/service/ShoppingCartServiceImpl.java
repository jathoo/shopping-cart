package com.shopping.cart.service;

import com.shopping.cart.model.BillingDetail;
import com.shopping.cart.model.Cart;
import com.shopping.cart.model.Offer;
import com.shopping.cart.model.Product;
import com.shopping.cart.service.IService.ProductService;
import com.shopping.cart.service.IService.ShoppingCartService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private final ProductService productService;

    public ShoppingCartServiceImpl(ProductService productService) {
        this.productService = productService;
    }


    @Override
    public Cart addProdcutsToCart(String[] productNames) {
        Cart cart = new Cart();
        for (String productName : productNames) {
            Optional<Product> product = productService.getProduct(productName);
            product.ifPresent(cart::addProduct);
        }
        return cart;
    }

    @Override
    public BillingDetail calculateCartPrice(Cart cart) {
        BillingDetail billingDetail = new BillingDetail();
        Map<Product, Integer> numberOfProductsInBasket = new HashMap<>();


        BigDecimal subTotal = calculateSubTotal(cart, numberOfProductsInBasket);
        Map<Offer, BigDecimal> OfferTotal = calculateOfferTotals(cart, numberOfProductsInBasket);


        billingDetail.setSubTotal(subTotal);
        billingDetail.setOfferTotals(OfferTotal);
        billingDetail.setTotal(calculateTotal(subTotal, OfferTotal));

        return billingDetail;
    }


    private BigDecimal calculateTotal(BigDecimal subTotal, Map<Offer, BigDecimal> offerTotalMap) {
        BigDecimal discountTotal = BigDecimal.ZERO;

        for (BigDecimal total : offerTotalMap.values()) {
            discountTotal = discountTotal.add(total);
        }
        return subTotal.subtract(discountTotal);
    }


    private Map<Offer, BigDecimal> calculateOfferTotals(Cart cart, Map<Product, Integer> numberOfProductsInBasket) {
        Map<Offer, BigDecimal> totalPerOffer = new HashMap<>();
        cart.getProducts().forEach(product -> {
            List<Offer> offers = productService.getOffers(product);
            offers.forEach(offer -> addOfferTotal(offer, numberOfProductsInBasket, totalPerOffer));
        });

        return totalPerOffer;
    }

    private void addOfferTotal(Offer offer, Map<Product, Integer> numberOfProductsInBasket, Map<Offer, BigDecimal> totalPerOffer) {
        if (offer.canApplyOffer(numberOfProductsInBasket)) {
            BigDecimal offerAmount = offer.calculateOfferAmount();
            BigDecimal totalOfferDiscount = totalPerOffer.get(offer);

            if (totalOfferDiscount == null) {
                totalPerOffer.put(offer, offerAmount);
            } else {
                totalPerOffer.put(offer,
                        totalOfferDiscount.add(offerAmount));
            }
        }
    }


    private BigDecimal calculateSubTotal(Cart cart, Map<Product, Integer> numberOfProductsInBasket) {
        BigDecimal subtotal = BigDecimal.ZERO;
        for (Product product : cart.getProducts()) {
            subtotal = subtotal.add(product.getPrice());
            numberOfProductsInBasket.merge(product, 1, Integer::sum);
        }
        return subtotal;
    }

}
