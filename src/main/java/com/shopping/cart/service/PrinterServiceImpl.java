package com.shopping.cart.service;

import com.shopping.cart.model.BillingDetail;
import com.shopping.cart.model.Offer;
import com.shopping.cart.service.IService.PrinterService;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

@Service
public class PrinterServiceImpl implements PrinterService {
    @Override
    public void print(BillingDetail billingDetail) {
        System.out.println("subtotal - " + toCurrency(billingDetail.getSubTotal()));
        System.out.println(prettyPrintOffers(billingDetail.getOfferTotals()));
        System.out.println("total - " + toCurrency(billingDetail.getTotal()));

    }

    public String prettyPrintOffers(Map<Offer, BigDecimal> offerTotals) {
        StringBuilder stringBuilder = new StringBuilder();
        if (offerTotals == null || offerTotals.isEmpty()) {
            stringBuilder.append("no offers available").append('\n');
        } else {
            offerTotals.forEach((offer, totalDiscount) -> {
                String formattedTotalDiscount = toCurrency(totalDiscount);
                stringBuilder.append(offer.messageBuilder(formattedTotalDiscount)).append('\n');
            });
        }
        return stringBuilder.toString();
    }

    public String toCurrency(BigDecimal value) {
            Locale locale = LocaleContextHolder.getLocale();
            NumberFormat numberFormatter = NumberFormat
                    .getCurrencyInstance(locale);
            return numberFormatter.format(value);

    }
}
