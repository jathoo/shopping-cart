package com.shopping.cart.service.IService;

import com.shopping.cart.model.BillingDetail;

public interface PrinterService {
    void print(BillingDetail billingDetail);
}
