package com.shopping.cart.service.IService;

import com.shopping.cart.model.Cart;
import com.shopping.cart.model.BillingDetail;

public interface ShoppingCartService {

    Cart addProdcutsToCart(String[] cart);
    BillingDetail calculateCartPrice(Cart cart);
}
