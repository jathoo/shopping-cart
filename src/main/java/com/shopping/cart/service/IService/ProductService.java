package com.shopping.cart.service.IService;

import com.shopping.cart.model.Product;
import com.shopping.cart.model.Offer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ProductService {
    Optional<Product> getProduct(String name);
    List<Offer> getOffers(Product product);
}
