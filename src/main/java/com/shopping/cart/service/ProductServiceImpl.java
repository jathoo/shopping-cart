package com.shopping.cart.service;

import com.shopping.cart.model.Offer;
import com.shopping.cart.model.Product;
import com.shopping.cart.service.IService.ProductService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.inject.Named;
import java.util.*;

@Service
@Named
public class ProductServiceImpl implements ProductService, ApplicationContextAware {


    public Map<String, Product> products = new HashMap<>();
    public Map<String, Offer> offers = new LinkedHashMap<>();

    @Override
    public Optional<Product> getProduct(String name) {
        return Optional.ofNullable(products.get(name.toLowerCase()));
    }

    @Override
    public List<Offer> getOffers(Product product) {
        List<Offer> offersForProduct = new LinkedList<>();
        offers.values().forEach(offer -> {
            if (product.equals(offer.getProduct())) { offersForProduct.add(offer); }
        });
        return offersForProduct;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        products = applicationContext.getBeansOfType(Product.class);
        offers = applicationContext.getBeansOfType(Offer.class);
    }

}
