package com.shopping.cart;

import com.shopping.cart.service.IService.ShoppingCartService;
import com.shopping.cart.service.IService.PrinterService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan(basePackages = "com.shopping")
@ImportResource("classpath:config.xml")
public class ShoppingCartManager {

    private final ShoppingCartService shoppingCartService;
    private final PrinterService printerService;

    private String[] productNames;

    public ShoppingCartManager(ShoppingCartService shoppingCartService, PrinterService printerService) {
        this.shoppingCartService = shoppingCartService;
        this.printerService = printerService;
    }


    public void execute() {
        printerService.print(shoppingCartService.calculateCartPrice(shoppingCartService.addProdcutsToCart(productNames)));

    }

    public void setProductNames(String[] products) {
        this.productNames = products;
    }

}
