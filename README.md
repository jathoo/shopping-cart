How to run
-----------

- make sure you have  gradle and java installed. 
- clone the repository 

build using gradle 
<pre>
./gradlew build
</pre>

Run the application EG:
<pre>
java -jar ./build/libs/CartApplication-0.0.1.jar apple soup milk soup bread
</pre>

prices and offers are configured as per the exercise : please find it in config.xml

for more information : build.gradle
 
Assumptions
-----------

- Invalid products will be ignored
- products are case insensitive 